$(function() {
   initTable();
   $('#toss').click(function() {
      makeToss();
   });
});

function clearTable(clearContent = false) {
   $('.game').each(function() {
      clearTableCell(this, clearContent);
   });
}
function clearTableCell(gameElem, clearContent) {
   $(gameElem)
       .removeClass('is_in_play is_played is_one_team')
       .off();
   if (clearContent) {
      $(gameElem).find('.name').text('');
      $(gameElem).find('.goals').text('');
   }
}

function initTable() {
   $.getJSON("/games/?method=getPlayers", function(data) {
      fillTable(data);
   });
}

function makeToss() {
      clearTable(true);
      $.getJSON("/games/?method=makeToss", function(data) {
         fillTable(data);
         getTotalTable();
      });
}

function fillTable(games) {
   games.forEach(function(game) {
      let gameElem = $('#game_' + game.game_id);
      let team_ids = game.team_ids.split(',');
      let names = game.names.split(',');
      let goals = game.goals ? game.goals.split(',') : [null, null];
      clearTableCell(gameElem);
      for ($i = 0; $i < team_ids.length; $i++) {
         let playerElem = $(gameElem).find('.player').eq($i);
         $(playerElem).find('.name').text(names[$i]);
         if (goals[$i]) {
            $(playerElem).find('.goals').text(goals[$i]);
         } else {
            $(playerElem).find('.goals').text('-');
         }
      }
      if (team_ids.length == 2) {
         if (goals[0]) {
            // Игра сыграна
            $(gameElem)
                .addClass('is_played');
         } else {
            // Игра не сыграна
            $(gameElem)
                .addClass('is_in_play')
                .click(function () {
                   askGoals(game.game_id, team_ids, names);
                });
         }
      } else if (team_ids.length == 1) {
         // Определена одна команда
         $(gameElem)
             .addClass('is_one_team');
      }
   });
}

function askGoals(game_id, team_ids, names) {
   let dialogStr =
      `<form id='player_form' action='/games/?method=setGoals'>
            <input class='hidden' type='text' name='game_id' value='${game_id}'>
        <div>
            <label class='player_label' for="goals_player_1">${names[0]}</label>
            <input id='goals_player_1' class='player_input' type='text' name='${team_ids[0]}' value='0'>
        </div>
        <div>
            <label class='player_label' for="goals_player_2">${names[1]}</label>
            <input id='goals_player_2' class='player_input' type='text' name='${team_ids[1]}' value='0'>
        </div>
      </form>`;
   $(dialogStr).dialog({
      modal: true,
      title: "Кто сколько забил?",
      buttons: {
         'Ok': function () {
            // TODO - валидация
            // Но на ничью проверим
            if ($('#goals_player_1').val() == $('#goals_player_2').val()) {
               alert("В play-off не может быть ничьей!");
               return;
            }
            $.post('/games/?method=setGoals', $('#player_form').serialize(), function(data) {
               fillTable(JSON.parse(data));
               getTotalTable();
            });
            $(this).dialog('destroy').remove();
         },
         'Отмена': function () {
            $(this).dialog('destroy').remove();
         }
      }
   });
}

function getTotalTable() {
   $.get("/games/?method=getTotalTable", function(data) {
      // totalTable рендерится на стороне сервера
      $("#total_table").html(data);
   });
}