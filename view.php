<?
class View {
   const BLOCK_MARGIN = 0.15; // Горизонтальные поля блоков в % от ширины элемента
   const GAME_HEIGHT = 5; // Высота блока Игра в % от высоты контейнера

   protected $stagesCount;
   protected $itemFullWidth;
   protected $itemWidth;
   protected $itemMargin;

   protected $templates = [
      "stage" => "<div class='item stage' style='%style'>%stage</div>",
      "game" => "<div id='%game_id' class='item game' style='%style'>%player%player</div>",
      "player" => "<div class='player'><div class='name'></div><div class='goals'></div></div>"
   ];

   public function __construct($stagesCount) {
      $this->stagesCount = $stagesCount;
      $this->itemFullWidth = 100 / ($this->stagesCount * 2 - 1); // Столбец с финалом один
      $this->itemMargin = $this->itemFullWidth * View::BLOCK_MARGIN;
      $this->itemWidth = $this->itemFullWidth - 2 * $this->itemMargin;
   }

   protected function getStagesBlock($stages) {
      $block = "";
      $params = [];
      foreach ($stages as $stage) {
         // Горизонтально смещение стадии для абсолютного позиционирования
         $offset = ($this->stagesCount - $stage->number) * $this->itemFullWidth + $this->itemMargin;
         $params['%stage'] = $stage->stage;

         $params['%style'] = sprintf("width: %f%%; height: %f%%; left: %f%%;", $this->itemWidth, View::GAME_HEIGHT, $offset);
         $block .= str_replace(array_keys($params), array_values($params), $this->templates['stage']);

         // Продублируем для правой части таблицы, но не для стадии Финал (она в центре)
         if ($stage->number != 1) {
            $params['%style'] = sprintf("width: %f%%; height: %f%%; right: %f%%;", $this->itemWidth, View::GAME_HEIGHT, $offset);
            $block .= str_replace(array_keys($params), array_values($params), $this->templates['stage']);
         }
      }
      return $block;
   }

   protected function getGamesBlock($games) {
      $block = "";
      $params = [];

      foreach ($games as $game) {
         // Максимальное кол-во игр в половине стадии
         $maxGamesInHalf = pow(2, $game->stage_number - 2);
         // Первую половину игр в стадии рисуем слева от финала, вторую - справа
         $isLeft = ($game->stage_number == 1 ? true : $game->number <= $maxGamesInHalf);
         // Номер игры в "своей" половине стадии
         $gameNumberInHalf = $isLeft ? $game->number : $game->number - $maxGamesInHalf;

         // Смещение игры для абсолютного позиционирования по горизонтали
         $offsetX = ($this->stagesCount - $game->stage_number) * $this->itemFullWidth + $this->itemMargin;

         // Смещение игры для абсолютного позиционирования по вертикали
         if ($game->stage_number != 1) {
            // Обычные игры
            $heightForOneGame = 100 / $maxGamesInHalf;
            $offsetY = $heightForOneGame * ($gameNumberInHalf - 0.5);
         } else {
            // Финал и игра за 3-место
            // Финал - 50% по высоте, игра за 3-е место - ниже на например три GAME_HEIGHT
            $offsetY = 50 + ($gameNumberInHalf - 1) * 3 * View::GAME_HEIGHT;
         }
         $offsetY -= 0.5 * View::GAME_HEIGHT;

         $params['%style'] = sprintf("width: %f%%; height:%f%%; %s: %f%%; top: %f%%",
            $this->itemWidth,
            2 * View::GAME_HEIGHT,
            $isLeft ? 'left' : 'right',
            $offsetX,
            $offsetY);
         $params['%game_id'] = 'game_'.$game->game_id;
         $params['%player'] = $this->templates['player'];
         $block .= str_replace(array_keys($params), array_values($params), $this->templates['game']);
      }
      return $block;
   }

   public function getTotalTableBlock($total_table) {
      // TODO - сделать шаблон таблицы
      $place = 1;

      $block = "<table class='ui-widget'>";
      foreach ($total_table as $row) {
         if ($place == 1) {
            $block .= "<thead class='ui-widget-header'><tr>";
            $block .= "<td>Место</td>";
            foreach ($row as $key => $value) {
               $block .= "<td>$key</td>";
            }
            $block .= "</tr></thead><tbody class='ui-widget-content'>";
            $firstRow = false;
         }
         $block .= "<tr>";
         $block .= "<td>$place</td>";
         foreach ($row as $value) {
            $block .= "<td>$value</td>";
         }
         $block .= "</tr>";
         $place++;
      }
      $block .= "</tbody></table>";
      return $block;
   }

   public function index($stages, $games, $total_table) {
      $stages_block = $this->getStagesBlock($stages);
      $games_block = $this->getGamesBlock($games);
      $total_table_block = $this->getTotalTableBlock($total_table);

	   $result = file_get_contents('index.tmpl');

      $result = str_replace('{stages_block}', $stages_block, $result);
      $result = str_replace('{games_block}', $games_block, $result);
      $result = str_replace('{total_table_block}', $total_table_block, $result);

	   return $result;
	}

	public function getTotalTable($total_table) {
      $total_table_block = $this->getTotalTableBlock($total_table);
      return $total_table_block;
	}
}
?>
