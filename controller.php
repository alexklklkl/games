<?
require_once "model.php";
require_once "view.php";

class Controller {

   protected $model;
   protected $view;

   protected $method;
   protected $allowed_methods = [
      'index',
      'makeToss',
      'setGoals',
      'getPlayers',
      'getTotalTable'
   ];

   public function __construct() {
      $this->method = 'index';
      if (isset($_REQUEST['method']))
         $method = $_REQUEST['method'];
      if (in_array($method, $this->allowed_methods) && (method_exists($this, $method)))
         $this->method = $method;

      $this->model = new Model();
      $stagesCount = $this->model->getStagesCount();
      $this->view = new View($stagesCount);
   }

   public function dispatch() {
      return $this->{$this->method}();
   }

   protected function getPlayers() {
      return json_encode($this->model->getPlayers());
   }

   protected function index() {
      $stages = $this->model->getStages();
      $games = $this->model->getGames();
      $total_table = $this->model->getTotalTable();

		$view = $this->view->index($stages, $games, $total_table);
      return $view;
   }

   protected function makeToss() {
      $this->model->makeToss();
      return $this->getPlayers();
   }

   protected function setGoals() {
      $data = $_POST;
      $game_id = intval($data['game_id']);
      unset($data['game_id']);

      $this->model->setGoals($game_id, $data);
      return $this->getPlayers();
   }

   protected function getTotalTable() {
      $total_table = $this->model->getTotalTable();
      return $this->view->getTotalTable($total_table);
   }
}
?>