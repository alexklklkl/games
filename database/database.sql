-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: park-kosa.mysql
-- Время создания: Сен 25 2019 г., 07:13
-- Версия сервера: 5.6.41-84.1
-- Версия PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `park-kosa_duny`
--

-- --------------------------------------------------------

--
-- Структура таблицы `g_games`
--
-- Создание: Сен 23 2019 г., 14:29
-- Последнее обновление: Сен 23 2019 г., 14:29
--

DROP TABLE IF EXISTS `g_games`;
CREATE TABLE `g_games` (
  `game_id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `g_games`
--

INSERT INTO `g_games` (`game_id`, `stage_id`, `number`) VALUES
(1, 4, 1),
(2, 4, 2),
(3, 4, 3),
(4, 4, 4),
(5, 4, 5),
(6, 4, 6),
(7, 4, 7),
(8, 4, 8),
(9, 3, 1),
(10, 3, 2),
(11, 3, 3),
(12, 3, 4),
(13, 2, 1),
(14, 2, 2),
(15, 1, 1),
(16, 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `g_stages`
--
-- Создание: Сен 20 2019 г., 06:34
-- Последнее обновление: Сен 20 2019 г., 06:34
--

DROP TABLE IF EXISTS `g_stages`;
CREATE TABLE `g_stages` (
  `stage_id` int(11) NOT NULL,
  `stage` varchar(100) CHARACTER SET cp1251 NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `g_stages`
--

INSERT INTO `g_stages` (`stage_id`, `stage`, `number`) VALUES
(1, 'Финал', 1),
(2, 'Полуфинал', 2),
(3, '1/4 финала', 3),
(4, '1/8 финала', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `g_teams`
--
-- Создание: Сен 24 2019 г., 09:33
-- Последнее обновление: Сен 25 2019 г., 07:11
--

DROP TABLE IF EXISTS `g_teams`;
CREATE TABLE `g_teams` (
  `team_id` int(11) NOT NULL,
  `team` varchar(100) CHARACTER SET cp1251 NOT NULL,
  `place` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `g_teams`
--

INSERT INTO `g_teams` (`team_id`, `team`, `place`) VALUES
(1, 'Россия', NULL),
(2, 'Франция', NULL),
(3, 'Канада', NULL),
(4, 'США', NULL),
(5, 'Англия', NULL),
(6, 'Италия', NULL),
(7, 'Аргентина', NULL),
(8, 'Бразилия', NULL),
(9, 'Испания', NULL),
(10, 'Португалия', NULL),
(11, 'Германия', NULL),
(12, 'Алжир', NULL),
(13, 'Нигерия', NULL),
(14, 'Уругвай', NULL),
(15, 'Чили', NULL),
(16, 'Колумбия', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `g_team_to_games`
--
-- Создание: Сен 17 2019 г., 18:35
-- Последнее обновление: Сен 25 2019 г., 07:12
--

DROP TABLE IF EXISTS `g_team_to_games`;
CREATE TABLE `g_team_to_games` (
  `team_to_game_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `goals` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `g_games`
--
ALTER TABLE `g_games`
  ADD PRIMARY KEY (`game_id`);

--
-- Индексы таблицы `g_stages`
--
ALTER TABLE `g_stages`
  ADD PRIMARY KEY (`stage_id`);

--
-- Индексы таблицы `g_teams`
--
ALTER TABLE `g_teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Индексы таблицы `g_team_to_games`
--
ALTER TABLE `g_team_to_games`
  ADD PRIMARY KEY (`team_to_game_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `g_games`
--
ALTER TABLE `g_games`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `g_stages`
--
ALTER TABLE `g_stages`
  MODIFY `stage_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `g_teams`
--
ALTER TABLE `g_teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `g_team_to_games`
--
ALTER TABLE `g_team_to_games`
  MODIFY `team_to_game_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
