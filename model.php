<?
require_once "db.php";

class Model {

   protected $pdo;
   protected $stagesCount;

   public function __construct() {
      $this->pdo = new PDO(DB::$dsn, DB::$user, DB::$password);
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  		$this->pdo->exec("set names utf8");
   }

   public function clear() {
      $query = <<<EOL
         DELETE FROM g_team_to_games
EOL;
      $rows = $this->pdo->prepare($query);
      $rows->execute();

      $query = <<<EOL
         UPDATE g_teams
         SET place = null
EOL;
      $rows = $this->pdo->prepare($query);
      $rows->execute();
   }

   public function getStagesCount() {
      if (!$this->stagesCount) {
         $query = <<<EOL
	   	SELECT
	   		count(1) AS stagesCount
	   	FROM
	   		g_stages
EOL;
         $rows = $this->pdo->prepare($query);
         $rows->execute();

         $result = $rows->fetch(PDO::FETCH_OBJ);
         $this->stagesCount = $result->stagesCount;
      }
		return $this->stagesCount;
   }

   public function getStages() {
	   $query = <<<EOL
	   	SELECT
	   		stage_id,
	   		stage,
	   		number
	   	FROM
	   		g_stages
EOL;
	   $rows = $this->pdo->prepare($query);
		$rows->execute();

		$result = $rows->fetchAll(PDO::FETCH_OBJ);
		return $result;
   }

   public function getGames() {
      $query = <<<EOL
	   	SELECT
	   		g_games.game_id,
	   		g_games.number,
	   		g_stages.number AS stage_number
	   	FROM
	   		g_games
	   		INNER JOIN g_stages on g_stages.stage_id = g_games.stage_id
EOL;
	   $rows = $this->pdo->prepare($query);
		$rows->execute();

		$result = $rows->fetchAll(PDO::FETCH_OBJ);
		return $result;
   }

   public function getPlayers() {
      // TODO - не нравится такой запрос, переделать
      $query = <<<EOL
         SELECT
             g_team_to_games.game_id,
             group_concat(g_team_to_games.team_id ORDER BY g_team_to_games.team_to_game_id) AS team_ids,
             group_concat(g_team_to_games.goals ORDER BY g_team_to_games.team_to_game_id) AS goals,
             group_concat(g_teams.team ORDER BY g_team_to_games.team_to_game_id) AS names
         FROM
             g_team_to_games
             INNER JOIN g_teams on g_teams.team_id = g_team_to_games.team_id
         GROUP BY g_team_to_games.game_id
EOL;
      $rows = $this->pdo->prepare($query);
      $rows->execute();

		$result = $rows->fetchAll(PDO::FETCH_OBJ);
		return $result;
   }

   // Жеребьевка
   public function makeToss() {
      $this->clear();
      
      $teamsCount = pow(2, $this->getStagesCount());
      $teams = range(1, $teamsCount);
      shuffle($teams);

      $query = <<<EOL
         INSERT INTO g_team_to_games
            (team_id, game_id)
         VALUES
            (
               :team_id,
               (
                  SELECT game_id
                  FROM g_games
                     INNER JOIN g_stages ON g_stages.stage_id = g_games.stage_id
                  WHERE
                     g_stages.number = :stage_number
                     AND g_games.number = :game_number
               )
            )
EOL;
      $rows = $this->pdo->prepare($query);
      // TODO - сделать одним запросом,
      for ($i = 0; $i < $teamsCount; $i++) {
         $rows->execute([
            'team_id' => $teams[$i],
            'stage_number' => $this->getStagesCount(),
            'game_number' => ceil(($i + 1) / 2)
         ]);
      }
   }

   // Результаты
   public function setGoals($game_id, $game_results) {
      $this->setGoalsForGame($game_id, $game_results);

      // Определим победителя и проигравшего (для двух можно и напрямую сравнить)
      $winner_id = array_keys($game_results, max($game_results))[0];
      $looser_id = array_keys($game_results, min($game_results))[0];
      $this->movePlayersToNextStage($game_id, $winner_id, $looser_id);
   }

   // Победитель переходит на следующую стадию, если полуфинал - проигравший играет за 3-е место
   protected function movePlayersToNextStage($game_id, $winner_id, $looser_id) {
      $numbers = $this->getGameNumbersById($game_id);
		if ($numbers->stage_number == 1) {
		   // Уже финал. TODO - сообщить об этом пользователю
		   if ($numbers->game_number == 1) {
            $this->setPlace($winner_id, 1); // 1-е место
            $this->setPlace($looser_id, 2);
		   } else {
            $this->setPlace($winner_id, 3);
            $this->setPlace($looser_id, 4);
		   }
		   return;
		} elseif ($numbers->stage_number == 2) {
		   // Полуфинал
         $this->setPlace($winner_id, 2); // 2-место точно есть
         $this->setPlace($looser_id, 4); // 4-место точно есть
         // Проигравшего - играть за 3-е место,
         // это всегда 2-я игра в 1-й стадии
         $this->moveTeamToStage($looser_id, 1, 2);
      }
		// Следующая стадия для победителя
		$new_stage_number = $numbers->stage_number - 1;
		// Можно сделать parent_game_id, но дерево бинарное, можно и так:
		$new_game_number = ceil($numbers->game_number / 2);

		$this->moveTeamToStage($winner_id, $new_stage_number, $new_game_number);
   }

   protected function setPlace($team_id, $place) {
      $query = <<<EOL
         UPDATE g_teams
         SET place = :place
         WHERE
            team_id = :team_id
EOL;
      $rows = $this->pdo->prepare($query);
      $rows->execute(['team_id' => $team_id, 'place' => $place]);
   }

   protected function setGoalsForGame($game_id, $results) {
      $query = <<<EOL
         UPDATE g_team_to_games
         SET goals = :goals
         WHERE
            game_id = :game_id
            AND team_id = :team_id
EOL;
      $rows = $this->pdo->prepare($query);
      foreach ($results AS $team_id => $goals) {
         $rows->execute(['game_id' => $game_id, 'team_id' => intval($team_id), 'goals' => intval($goals)]);
      }
   }

   protected function moveTeamToStage($team_id, $stage_number, $game_number) {
      $query = <<<EOL
         INSERT INTO g_team_to_games
            (team_id, game_id)
         VALUES
            (
               :team_id,
               (
                  SELECT game_id
                  FROM g_games
                     INNER JOIN g_stages ON g_stages.stage_id = g_games.stage_id
                  WHERE
                     g_stages.number = :stage_number
                     AND g_games.number = :game_number
               )
            )
EOL;
      $rows = $this->pdo->prepare($query);
      $rows->execute(['team_id' => $team_id, 'stage_number' => $stage_number, 'game_number' => $game_number]);
   }

   protected function getGameNumbersById($game_id) {
      $query = <<<EOL
         SELECT
            g_games.number AS game_number,
            g_stages.number AS stage_number
         FROM
            g_games
            INNER JOIN g_stages ON g_stages.stage_id = g_games.stage_id
         WHERE
            g_games.game_id = :game_id
EOL;
      $rows = $this->pdo->prepare($query);
      $rows->execute(['game_id' => $game_id]);
      $result = $rows->fetch(PDO::FETCH_OBJ);
      return $result;
   }

   public function getTotalTable() {
      // Неудачный хостинг - нет хранимых процедур,
      // сделаем подзапросами

      // Игры с результатами
      $cte_games = <<<EOL
         (SELECT
            tg1.team_id,
            t1.team AS team1,
            tg1.goals AS goals1,
            tg2.goals AS goals2,
            (tg1.goals - tg2.goals) AS result
         FROM g_team_to_games tg1
            INNER JOIN g_teams t1 on t1.team_id = tg1.team_id
            INNER JOIN g_team_to_games tg2 on tg2.game_id = tg1.game_id
            INNER JOIN g_teams t2 on t2.team_id = tg2.team_id
         WHERE
            tg1.team_id <> tg2.team_id
            AND not isnull(tg1.goals)) AS cte_games
EOL;
      // Игры
      $cte_all = <<<EOL
         (SELECT
            cte_games.team_id,
            count(cte_games.team_id) AS gameCount,
            sum(cte_games.goals1) AS putedGoals,
            avg(cte_games.goals1) AS avgPutedGoals,
            max(cte_games.goals1) AS maxPutedGoals,
            sum(cte_games.goals2) AS missedGoals,
            sum(cte_games.result) AS difGoals
         FROM
            $cte_games
         GROUP BY
            cte_games.team_id) AS cte_all
EOL;
      // Победы
      $cte_wins = <<<EOL
         (SELECT
            cte_games.team_id,
            count(cte_games.team_id) AS wins
         FROM
            $cte_games
         WHERE
            cte_games.goals1 > cte_games.goals2
         GROUP BY
            cte_games.team_id) AS cte_wins
EOL;
      // Поражения
      $cte_looses = <<<EOL
         (SELECT
            cte_games.team_id,
            count(cte_games.team_id) AS looses
         FROM
            $cte_games
         WHERE
            cte_games.goals1 < cte_games.goals2
         GROUP BY
            cte_games.team_id) AS cte_looses
EOL;
      // Итоговый запрос
      $query = <<<EOL
         SELECT
            g_teams.team AS "Команда",
            cte_all.gameCount AS "Сыграно",
            cte_wins.wins AS "Выйграно",
            cte_looses.looses AS "Проиграно",
            cte_all.putedGoals AS "Забито",
            cte_all.missedGoals AS "Пропущено",
            cte_all.difGoals AS "Разница",
            cte_all.avgPutedGoals AS "Средний рез.",
            cte_all.maxPutedGoals AS "Макс. рез."
         FROM g_teams
            LEFT JOIN
               $cte_all ON g_teams.team_id = cte_all.team_id
            LEFT JOIN
               $cte_wins ON g_teams.team_id = cte_wins.team_id
            LEFT JOIN
               $cte_looses ON g_teams.team_id = cte_looses.team_id
         ORDER BY (4 - g_teams.place) desc, cte_looses.looses asc, cte_wins.wins desc, cte_all.gameCount asc, cte_all.difGoals desc, cte_all.putedGoals desc, cte_all.missedGoals asc
EOL;
      $rows = $this->pdo->prepare($query);
      $rows->execute();
      $result = $rows->fetchAll(PDO::FETCH_ASSOC);
      return $result;
   }
}
?>